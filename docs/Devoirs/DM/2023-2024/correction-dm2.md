---
title:  Correction du DM n°2
---

[Version pdf](https://nuage03.apps.education.fr/index.php/s/3WQRWXsCmtcaBHT)

# Correction du DM n°2


On considère un réseau local N1 constitué de trois ordinateurs M1, M2, M3 et dont les adresses IP sont les suivantes :

* M1 : 192.168.1.1/24 ;
* M2 : 192.168.1.2/24 ;
* M3 : 192.168.2.3/24.
On rappelle que le "/24" situé à la suite de l"adresse IP de M1 signifie que l'adresse réseau du réseau local N1 est 192.168.1.0.
Depuis l'ordinateur M1, un utilisateur exécute la commande ping vers l'ordinateur M3 comme suit :

~~~python
util@M1 ~ % ping 192.168.2.3
PING 192.168.2.3 (192.168.2.3): 56 data bytes
Hôte inaccessible
~~~

## Question 1

La machine M3 dont l'IP en notation CIDR est 192.168.2.3/24, devrait appartenir au  sous-réseau d'adresse  192.168.2.0.  
Ce réseau n'est pas le même que celui de la machine M1 d'IP 192.168.1.1/24 dont le sous-réseau a pour adresse 192.168.1.0.  La commande `ping` envoie des paquets de test avec le protocole ICMP de l'émetteur vers le destinataire. Le résultat de la commande `ping 192.168.2.3`  exécuté depuis M1 indique que M3 n'est pas atteignable depuis M1. Comme  M3 n'est pas dans  le même réseau que M1 le paquet devrait être transmis à la passerelle par défaut de M1. Une passerelle est un hôte spécifique qui s'occupe du routage des paquets entre deux sous-réseaux interconnectés. L'échec de la commande indique que soit aucune passerelle n'est définie pour M1, soit une passerelle est définie mais sa table de routage ne permet pas d'atteindre le sous-réseau 192.168.2.0 de M3.


## Question 2

RAM signifie Random Access Memory et désigne la mémoire vive volatile dont toutes les cellules peuvent être accédées directement.


## Question 3

Linux désigne le noyau d'un système d'exploitation libre créé par Linus Torvalds au début des années 1990 et qui sert de base à de nombreux systèmes d'exploitations équipant des distributions logicielles libres comme Ubuntu, Mint, Fedora, Debian ...


## Question 4

Le rôle de routeur est de transmettre des paquets dont l'émetteur et le destinataire ne sont pas dans le même sous-réseau et donc d'interconnecter des sous-réseaux. Il faut donc qu'un routeur ait des interfaces dans au moins deux sous-réseaux.

## Question 5

![alt](https://nuage03.apps.education.fr/index.php/s/SYYg2TYX4Ergnw2/download/reseau1-dm2.png)

L'interface `etho` du routeur R1 jouera le rôle de passerelle pour le réseau N1. On peut lui attribuer l'adresse `192.168.1.254` qui est libre et qui est juste avant la plus haute de la plage  qui est réservée au  `broadcast`.

## Question 6


![alt](https://nuage03.apps.education.fr/index.php/s/jsttcZziot4sH6S/download/table-routage-rip.png)

Le protocole de routage RIP choisira le chemin avec le nombre minimum de sauts pour router un paquet à destination du réseau N2. 

D'après la table donnée, le chemin optimal pour RIP sera R1 -> R3 -> R4 avec deux sauts et le paquet sera émis sur l'interface `eth2`.


## Question 7

Le routeur R3 tombe en panne. Après quelques minutes, la table de routage de R1 est modifiée afin de tenir compte de cette panne.

|Destination|Interface de sortie|métrique|
|:---:|:---:|:---:|
|N1|eth0|0|
|N2|eth2|4|
|N3|eth2|3|

Le réseau N4 raccordé directement à R3 n'est plus atteignable.


## Question 8

Le routeur R3 est de nouveau fonctionnel. Dans la suite de cet exercice, on utilise le protocole de routage OSPF (Open Shortest Path First). On rappelle que dans ce protocole, la métrique de la table de routage correspond à la somme des coûts : 

coût = $\frac{10^{8}}{d}$ (où $d$ est la bande passante d'une laison en bit/s)

|Type de liaison|Débit en bit/s|Coût|
|:---:|:---:|:---:|
|Fibre|$10^{9}$|$0,1$|
|Fast-Ethernet|$10^{8}$|1|
|Ethernet|$10^{7}$|10|


## Question 9

Si le coût total d'un chemin de R1 vers N3 par l'interface  eth2 de R1 est 0,3 alors puisque le coût de la liaison R6 - R3 est  de 1, le seul chemin possible est R1 -> R2 -> R6 -> R5 avec un coût total de de $0,1 + x + 0,1$ si note $x$ le coût de liaison R2 -> R6.  La résolution de l'équation $0,1+x+0,1=0,3$ donne $x=0,1$. La liaison inter-routeur R2 -> R6 est donc de type Fibre.

![alt](https://nuage03.apps.education.fr/index.php/s/jp9M6tM6LZ6jCKn/download/reseau3-dm2.png)


## Question 10


|Destination|Interface de sortie|métrique|
|:---:|:---:|:---:|
|N1|eth0|0|
|N2|eth1|0,2|
|N2|eth2|1,3|
|N3|eth1|1,2|
|N3|eth2|0,3|
|N4|eth1|0,1|
|N4|eth2|1,2|


## Question 11

On ajoute un réseau local N5 et un routeur R7 au réseau étudié ci-dessus. Le routeur R7 possède trois interfaces réseaux eth0, eth1 et eth2. eth0 est directement relié au réseau local N5. eth1 et eth2 sont reliés à d'autres routeurs (ces liaisons   inter-routeur sont de type Fibre).

Les deux tableaux suivants présentent un extrait des tables de routage des routeurs R1 et R3 :

![alt](https://nuage03.apps.education.fr/index.php/s/dTHen7bHNaRGCb6/download/n5.png)

Schéma du réseau en ajoutant un routeur R7 et un réseau local N5 compatibles avec les indications précédentes.

![alt](https://nuage03.apps.education.fr/index.php/s/d3w93CsSSYjJTSs/download/reseau4-dm2.png)
