def nb_occurrences(lettre, mot):
    """
    Renvoie le nombre d'occurrences de lettre dans mot
    """
    if len(mot) == 0:
        return 0
    c = 0
    if mot[0] == lettre:
        c = 1
    return c + nb_occurrences(lettre, mot[1:])


assert nb_occurrences("a", "") == 0
assert nb_occurrences("a", "ab") == 1
assert nb_occurrences("a", "aba") == 2
assert nb_occurrences("a", "aaa") == 3