#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 12 22:43:43 2023

@author: fjunier
"""
class Maillon:
    def __init__(self, valeur):
        self.valeur = valeur
        self.suivant = None

class File:
    def __init__(self):
        self.tete = None
        self.fin = None

    def est_vide(self):
        return self.fin is None

    def affiche(self):
        maillon = self.tete
        print('←', end=' ')
        while maillon is not None:
            print(maillon.valeur, end=' ')
            maillon = maillon.suivant
        print('←')

    def enfile(self, element):
        nouveau_maillon = Maillon(element)
        if self.est_vide():
            self.tete = nouveau_maillon
        else:
            self.fin.suivant = nouveau_maillon
        self.fin = nouveau_maillon

    def defile(self):
        if not self.est_vide():
            resultat = self.tete.valeur
            self.tete = self.tete.suivant
            if self.tete is None:
                self.fin = None
            return resultat
        else:
            return None

# tests

file = File()
assert file.est_vide()
file.enfile(2)
assert not file.est_vide()
file.enfile(5)
file.enfile(7)
assert file.defile() == 2
assert file.defile() == 5