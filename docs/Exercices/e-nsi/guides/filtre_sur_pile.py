#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 21:46:26 2023

@author: fjunier
"""
# la classe Pile est utilisable directement pour cet exercice

class Pile:
    "Classe à ne pas modifier"
    def __init__(self, valeurs=None):
        """Initialise une pile
        - vide, si `valeurs` n'est pas fourni ;
        - remplie avec `valeurs` sinon.
            - Le sommet de la pile est à la fin de la liste.
        """
        if valeurs is None:
            self.valeurs = []
        else:
            self.valeurs = valeurs

    def est_vide(self):
        "Renvoie un booléen : la pile est-elle vide ?"
        return self.valeurs == []

    def __str__(self):
        "Affiche la pile, en indiquant le sommet"
        return "| " + " | ".join(map(str, self.valeurs)) + ' <- sommet'

    def depile(self):
        """
        - Si la pile est vide, provoque une erreur.
        - Sinon, dépile un élément au sommet et le renvoie.
        """
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def empile(self, element):
        "Empile un élément au sommet de la pile"
        self.valeurs.append(element)
        
def filtre_positifs(donnees):
    "fonction à compléter"
    auxiliaire = Pile()
    while not  donnees.est_vide():
        element = donnees.depile()
        auxiliaire.empile(element)
    
    resultat = Pile()
    while not  auxiliaire.est_vide():
        element = auxiliaire.depile()
        donnees.empile(element)
        if element >= 0:
            resultat.empile(element)
    
    return resultat





# tests

donnees = Pile([4, -11, 7, -3, -1, 0, 6])
assert str(filtre_positifs(donnees)) == '| 4 | 7 | 0 | 6 <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| 4 | -11 | 7 | -3 | -1 | 0 | 6 <- sommet'

donnees = Pile([1, 2, 3, 4])