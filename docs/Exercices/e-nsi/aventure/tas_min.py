#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 23:21:33 2024

@author: fjunier
"""


def est_tas_min(tableau):
    def aux(i):
        rep = True
        if 2 * i < len(tableau):
            rep = rep and (tableau[2 * i] >= tableau[i] and aux(2 * i))
        if 2 * i + 1 < len(tableau):
            rep = rep and (tableau[2 * i + 1] >= tableau[i] and aux(2 * i + 1))
        return rep

    if len(tableau) == 1:
        return True
    return aux(1)


def est_tas_min2(tableau):
    for i in range(2, len(tableau)):
        if tableau[i] < tableau[i // 2]:
            return False
    return True


# tests

## exemples
assert est_tas_min([None, 10, 42, 23, 55, 67]) == True

assert (
    est_tas_min(
        [
            None,
            "brrr",
            "chut",
            "ouille",
            "dring",
            "tada",
            "vroum",
            "wahou",
            "paf",
            "hehe",
        ]
    )
    == True
)

assert est_tas_min([None]) == True

assert est_tas_min([None, 10, 10]) == True

## contre-exemples
assert est_tas_min([None, 10, 2]) == False

assert est_tas_min([None, "ba", "ab"]) == False

assert est_tas_min([None, 10, 42, 23, 30]) == False

assert est_tas_min([None, 10, 42, 23, 55, 40]) == False
