#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 22:48:23 2023

@author: fjunier
"""
def caviarder(texte, debut, fin):
    res = ''
    for k in range(len(texte)):
        if debut <= k <= fin and texte[k].isalpha():
            res = res + '#'
        else:
            res = res + texte[k]
    return res


# Tests
assert caviarder("L'espion était J. Bond", 15, 21) == "L'espion était #. ####"
assert caviarder("Paul est un espion", 100, 200) == "Paul est un espion"

