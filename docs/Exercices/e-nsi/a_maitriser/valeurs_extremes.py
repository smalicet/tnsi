#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 21:28:21 2023

@author: fjunier
"""
def extremes(valeurs):
    if len(valeurs) == 0:
        return {'min': None, 'max': None}
    extremum = {'min': valeurs[0], 'max': valeurs[0]}
    for v in valeurs:
        if v < extremum['min']:
            extremum['min'] = v
        elif v > extremum['max']:
            extremum['max'] = v
    return extremum





# tests

assert extremes([0, 1, 4, 2, -2, 9, 3, 1, 7, 1]) == {'min': -2, 'max': 9}
assert extremes([37, 37]) == {'min': 37, 'max': 37}
assert extremes([]) == {'min': None, 'max': None}