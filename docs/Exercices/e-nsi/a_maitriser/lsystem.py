#!/usr/bin/env python3
# -*- coding: utf-8 -*-
def transformation(motif, regles):
    suivant = ""
    for c in motif:
        if c in regles:
            suivant = suivant + regles[c]
        else:
            suivant = suivant + c
    return suivant


def n_transformations(motif, regles, n):
    for _ in range(n):
        motif = transformation(motif, regles)
    return motif
        


# Tests
regles = {'a': 'ab', 'b': 'ac', 'c': 'd'}
motif = 'a'
assert transformation(motif, regles) == 'ab'
assert n_transformations(motif, regles, 2) == 'abac'
assert n_transformations(motif, regles, 3) == 'abacabd'
assert n_transformations(motif, regles, 5) == 'abacabdabacdabacabdd'
assert n_transformations("rien ne change !", {'z': 'y'}, 50) == 'rien ne change !'

