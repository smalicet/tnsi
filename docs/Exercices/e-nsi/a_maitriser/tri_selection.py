#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 21:37:05 2023

@author: fjunier
"""
def tri_selection(tableau):
    for i in range(len(tableau)):
        imin = i
        for j in range(i + 1, len(tableau)):
            if tableau[j] < tableau[imin]:
                imin = j
        tmp = tableau[i]
        tableau[i] = tableau[imin]
        tableau[imin] = tmp


# tests

tab = [1, 52, 6, -9, 12]
tri_selection(tab)
assert tab == [-9, 1, 6, 12, 52], "Exemple 1"

tab_vide = []
tri_selection(tab_vide)
assert tab_vide == [], "Exemple 2"

singleton = [9]
tri_selection(singleton)
assert singleton == [9], "Exemple 3"
