#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 21:10:56 2022

@author: fjunier
"""
def valeur_et_indice_du_max(valeurs):
    """à compléter"""
    if len(valeurs) == 0:
        return (None, None)
    vmax = valeurs[0]
    imax = 0
    for i in range(1, len(valeurs)):
        v = valeurs[i]
        if v > vmax:
            vmax = v
            imax = i
    return (vmax, imax)






# tests
assert valeur_et_indice_du_max([1, 5, 6, 9, 1, 2, 3, 7, 9, 8]) == (9, 3)
assert valeur_et_indice_du_max([1, 1, 1, 99, 99]) == (99, 3)
assert valeur_et_indice_du_max([10]) == (10, 0)
assert valeur_et_indice_du_max([]) == (None, None)
