def gelees(temperatures):
    lm = 0
    lc = 0
    for t in temperatures:
        if t > 0:
            if lc > lm:
                lm = lc
            lc = 0
        else:
            lc = lc + 1
    if lc > lm:
        lm = lc
    return lm
    




# Tests
# assert gelees([2, -3, -2, 0, 1, -1]) == 3
# assert gelees([3, 2, 2]) == 0
# assert gelees([]) == 0
