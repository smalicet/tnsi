---
title: Exercice sur les listes chaînées
author: Frédéric Junier
---



1.  On construit une liste chaînée `L` par :

    `L = ajoute(ajoute(ajoute(ajoute(creerListe(), 2),7),1),8)`

    1.  Donner le tuple qui représente la liste chaînée `L`.

    2.  Donner le tuple qui représente la liste chaînée obtenue par
        `queue(L)`.

    3.  Quel est le résultat de l'évaluation de `tete(queue(queue(L)))`
        ?

2.  En utilisant exclusivement les fonctions de l'implémentation qui a
    été rappelée en début d'exercice, écrire une fonction `somme(L)` qui
    renvoie la somme des éléments de la liste chaînée `L`.

    *Vous ne pouvez utiliser que les fonctions de l'implémentation
    fonctionnelle fournie dans l'énoncé, il ne s'agit pas d'une
    implémentation objet ou avec des listes Python.*
