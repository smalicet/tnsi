# --- HDR ---#


#%% Trace des appels récursifs

import sys


def trace(f):
    """Remplace une fonction f par une fonction enrobée g."""

    if hasattr(f, "trace"):
        return f

    def g(*args):
        """Fonction enrobante (wraped en anglais)."""

        arguments = f"{args[0]}"
        if g.trace_pile:
            g.pile.append(f"Contexte de {g.__name__}{tuple(e for e in args)}")
            lmax = max([len(contexte) for contexte in g.pile])
            print(f"Appel de {g.__name__}({arguments}) => descente \n")
            print("État de la pile : \n")
            for contexte in g.pile[::-1]:
                print("|{0:^{1}}|".format(contexte, lmax))
                print(f"|{lmax * '-'}|")
            print("\n" * 3)
        if not (g.trace_pile) and g.trace:
            print(
                "| " * (g.indentation - 1)
                + "┌"
                + f"Appel de {g.__name__}({arguments}) => descente"
            )
            g.indentation += 1
            g.__dict__["indentation"] = g.indentation
        retour = f(*args)
        if not (g.trace_pile) and g.trace:
            g.indentation -= 1
            g.__dict__["indentation"] = g.indentation
            print(
                "| " * (g.indentation - 1)
                + "└"
                + f"Fin de {g.__name__}({arguments}) <= remontée"
            )
        if g.trace_pile:
            print(f"Fin de {g.__name__ }({arguments}) <=  remontée \n")
            lmax = max([len(contexte) for contexte in g.pile])
            print("État de la pile : \n")
            for contexte in g.pile[::-1]:
                print("|{0:^{1}}|".format(contexte, lmax))
                print(f"|{lmax * '-'}|")
            print("\n" * 2)
            g.pile.pop()
        return retour

    g.__name__ = f.__name__
    g.indentation = 1  # compteur d'indentation attaché à g
    g.trace = True  # booléen pour activer ou désactiver la trace
    g.trace_pile = False
    g.pile = []
    return g


def trace_dot(f):
    """
    Transforme la fonction f en une fonction enrobée
    Génère une trace sous forme d'arbre d'appels au format dot

    """
    if hasattr(f, "piledot"):
        return f  # pas de probleme avec plusieurs f=trace(f)

    def g(*args):
        g.id = g.id + 1
        moi, parent = g.id, None if len(g.piledot) == 0 else g.piledot[-1]
        if g.tracedot and not parent:
            print("digraph G {", file=sys.stderr)
        g.piledot.append(g.id)
        res = f(*args)
        g.piledot.pop()
        if g.tracedot:
            if parent:
                print("{} -> {}".format(parent, moi), file=sys.stderr)
            print(
                '{} [label="{}{}={}"]'.format(moi, g.nom, args, res),
                file=sys.stderr,
            )
            if not parent:
                print("}", file=sys.stderr)
        return res

    g.id, g.piledot, g.tracedot, g.nom = 0, [], True, f.__name__
    return g


# --- HDR ---#


def pgcd_iteratif(a, b):
    while b != 0:
        tmp = a
        a = b
        b = tmp % b
    return a


def pgcd_recursif(a, b):
    if b == 0:  # cas de base
        return a
    else:
        return pgcd_recursif(b, a % b)  # réduction
