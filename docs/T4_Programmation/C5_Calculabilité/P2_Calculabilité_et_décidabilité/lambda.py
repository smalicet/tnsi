affine = lambda x: x + 4
compose = (lambda f: lambda g: lambda x: g(f(x)))
f1 = lambda z: z+5
g1 = lambda y: 4*y

fixed_point_combinator = lambda f : (lambda x : f(lambda v : x(x)(v)))(lambda x : f(lambda v : x(x)(v)))
triangle = fixed_point_combinator(lambda t: lambda n: 0 if n == 0 else n  + t(n - 1))