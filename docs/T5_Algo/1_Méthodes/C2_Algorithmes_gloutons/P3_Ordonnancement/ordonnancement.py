import random

def tri_taches(liste_taches, clef):
    """Renvoie le tri de liste_taches 
    selon la fonction de clef de tri"""
    return sorted(liste_taches, key=clef)

def critere_ratio_glouton(tache):
    """Renvoie pour la tache qui est un couple (longueur, priorite)
    la valeur du quotient longueur / priorite
    """
    longueur, priorite = tache
    return longueur / priorite

def critere_diff_glouton(tache):
    """Renvoie pour la tache qui est un couple (longueur, priorite)
    la valeur de la différence longueur - priorite
    """
    longueur, priorite = tache
    return longueur - priorite

def objectif(ordo_taches):
    """
    Renvoie la valeur de la fonction objectif pour une liste 
    de taches (des couples (longueur, priorite) )
    La fonction objectif est la somme  des  temps de complétion pondérés
    par les priorités
    """
    temps_completion = 0
    somme = 0
    # à compléter (plusieurs lignes)
    ...
    return somme          


def ordonnancement_glouton(liste_taches, critere_glouton):
    """Renvoie le couple 
    (valeur de la fonction objectif, ordonnancement des taches selon le critere glouton)"""
    # à compléter (plusieurs lignes)
    ...


def test_ordonnancement_glouton():
    liste_taches1 = [(7, 2), (46, 3), (10, 6), (36, 10), (17, 6)]
    assert ordonnancement_glouton(liste_taches1, critere_ratio_glouton) == (1338, [(10, 6), (17, 6), (7, 2), (36, 10), (46, 3)])
    assert ordonnancement_glouton(liste_taches1, critere_diff_glouton) == (1346, [(10, 6), (7, 2), (17, 6), (36, 10), (46, 3)])
    print("tests réussis pour ordonnancement_glouton")
    
def comparaison(critere1, critere2, nb_exp):
    """
    Pour nb_exp listes de taches aléatoires
    Renvoie une liste res :
    
    res[0] est le nombre de fois où l'ordonnancement par critere1 et critere2 
    donnent la même valeur pour la fonction objectif
    
    res[1] est le nombre de fois où l'ordonnancement par critere1 est meilleur (plus petit)
    que celui par critere2
    
    res[2] est le nombre de fois où l'ordonnancement par critere1 est meilleur (plus petit)
    que celui par critere2    
    """
    res = [0, 0, 0]
    for _ in range(nb_exp):
        liste_taches = [(random.randint(1, 100), random.randint(1, 10)) for _ in range(50)]
        c1, _ =  ordonnancement_glouton(liste_taches, critere1)
        c2, _ =  ordonnancement_glouton(liste_taches, critere2)
        if c1 < c2:
            res[1] += 1
        elif c2 < c1:
            res[2] += 1
        else:
            res[0] += 1
    return res


#test_ordonnancement_glouton()
#comparaison(critere_ratio_glouton,  critere_diff_glouton, 100)