def permutations(n):
    """Renvoie toutes les permutations de la séquence d'entiers consécutifs [0, 1, 2, ..., n]"""
    if n == 0:
        return [[0]]
    return [...    for p in permutations(n - 1) for i in range(len(p) + 1)]
            
def test_permutations():
    assert sorted(permutations(1)) == [[0, 1], [1, 0]]
    assert sorted(permutations(2)) == [[0, 1, 2], [0, 2, 1], [1, 0, 2], [1, 2, 0], [2, 0, 1], [2, 1, 0]]
    print("tests réussis pour permutations")
    
    
class TSP1:
    """Classe pour Travel Salesman Problem"""
    
    def __init__(self, v, d):
        # liste de noms de villes
        self.nom_ville = v 
        # nombre de villes
        self.nb_villes = len(self.nom_ville) 
        # dictionnaire :  nom de ville -> index dans self.nom_ville
        self.index_ville = {self.nom_ville[k]: k  for k in range(self.nb_villes)}
        # tableau 2d des distances entre villes (repérées par leur index)
        self.distance = d        
        
    def distance_circuit(self, circuit):
        """Distance totale parcourue dans le circuit hamiltonien circuit
        qui est une liste d'indexs de villes dans self.nom_ville"""
        d = 0
        for k in range(len(circuit) - 1):
            d = d + self.distance[circuit[k]][circuit[k + 1]]
        return d
        
    def meilleur_circuit_bruteforce(self):
        """Renvoie le couple (distance minimale, circuit minimal)
        où circuit minimal est un circuit hamiltonien de distance minimale
        """
        dmin = float('inf')
        for p in permutations(self.nb_villes - 1):
            # c'est un circuit : ville de départ = ville de fin
            circuit = p + [p[0]]
            d = ...
            if ...:
                dmin = ...
                vmin = [self.nom_ville[v] for v in circuit]
        return (..., vmin)

        
def test_circuit_brute_force():
    nom_ville = ['Nancy', 'Metz', 'Paris', 'Reims', 'Troyes' ]
    distance = [[0, 55, 303, 188, 183], 
                [55, 0, 306, 176, 203], 
                [303, 306, 0, 142, 153],
                [188, 176, 142, 0, 123], 
                [183, 203, 153, 123, 0]]
    tsp1 = TSP1(nom_ville, distance)
    (dmin, vmin) = tsp1.meilleur_circuit_bruteforce()
    vmin_nancy = vmin[vmin.index('Nancy'):-1] + vmin[:vmin.index('Nancy') + 1]
    attendu = ['Nancy', 'Troyes', 'Paris', 'Reims', 'Metz', 'Nancy']
    assert dmin == 709 and  (vmin_nancy == attendu or vmin_nancy[::-1] == attendu)
    print("tests réussis pour circuit_brute_force")

#test_circuit_brute_force()