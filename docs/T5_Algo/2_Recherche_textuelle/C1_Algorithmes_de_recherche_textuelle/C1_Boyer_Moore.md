---
title: Algorithmes de recherche textuelle 🎯
---

# Algorithmes de recherche textuelle  (Bac 🎯)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme.png){: .center}


*  Algorithmes de recherche de motif dans un texte : _brute-force_, _Boyer-Moore version simplifiée de Horspool_ et _Boyer-Moore_ dans ce [TP](https://nuage03.apps.education.fr/index.php/s/Q6kpK4NCKzssL8S)
* [Matériel avec squelettes de codes et corrections](https://nuage03.apps.education.fr/index.php/s/733G6LidFyjKtnF)
