# Consignes pour le projet final

Le matériel peut être téléchargé sous la  forme d'une archive [`projet_final_2024.zip`](./materiel.zip).



Une fois l'archive déballée, on obtient l'arborescence suivante :
<pre><font color="#26A269"><b>fjunier@fjunier</b></font>:<font color="#12488B"><b>~/Projets/projet_final_2024_materiel</b></font>$ tree
<font color="#12488B"><b>.</b></font>
├── <font color="#12488B"><b>arbre_binaire</b></font>
│   └── <font color="#12488B"><b>eleves</b></font>
│       ├── attendu1.txt
│       ├── attendu2.txt
│       ├── attendu3.txt
│       ├── attendu4.txt
│       ├── attendu5.txt
│       ├── attendu6.txt
│       ├── input1.txt
│       ├── input2.txt
│       ├── input3.txt
│       ├── input4.txt
│       ├── input5.txt
│       ├── input6.txt
│       ├── output1.txt
│       ├── output2.txt
│       ├── output3.txt
│       ├── output4.txt
│       ├── output5.txt
│       ├── output6.txt
│       ├── test_tree_traversal.py
│       └── tree_traversal.py
├── <font color="#12488B"><b>automate_cellulaire</b></font>
│   └── <font color="#12488B"><b>eleves</b></font>
│       ├── attendu1.txt
│       ├── attendu2.txt
│       ├── attendu3.txt
│       ├── attendu4.txt
│       ├── attendu5.txt
│       ├── attendu6.txt
│       ├── automate.py
│       ├── input1.txt
│       ├── input2.txt
│       ├── input3.txt
│       ├── input4.txt
│       ├── input5.txt
│       ├── input6.txt
│       ├── output1.txt
│       ├── output2.txt
│       ├── output3.txt
│       ├── output4.txt
│       ├── output5.txt
│       ├── output6.txt
│       └── test_automate.py
├── <font color="#12488B"><b>labyrinthe</b></font>
│   └── <font color="#12488B"><b>eleves</b></font>
│       ├── attendu1.txt
│       ├── attendu2.txt
│       ├── attendu3.txt
│       ├── attendu4.txt
│       ├── attendu5.txt
│       ├── attendu6.txt
│       ├── attendu7.txt
│       ├── input1.txt
│       ├── input2.txt
│       ├── input3.txt
│       ├── input4.txt
│       ├── input5.txt
│       ├── input6.txt
│       ├── input7.txt
│       ├── laby.py
│       ├── output1.txt
│       ├── output2.txt
│       ├── output3.txt
│       ├── output4.txt
│       ├── output5.txt
│       └── test_laby.py
├── <font color="#12488B"><b>loup_chevre_chou</b></font>
│   └── <font color="#12488B"><b>eleves</b></font>
│       ├── attendu1.txt
│       ├── attendu2.txt
│       ├── attendu3.txt
│       ├── attendu4.txt
│       ├── attendu5.txt
│       ├── attendu6.txt
│       ├── input1.txt
│       ├── input2.txt
│       ├── input3.txt
│       ├── input4.txt
│       ├── input5.txt
│       ├── input6.txt
│       ├── loup_chevre_chou.py
│       ├── output1.txt
│       ├── output2.txt
│       ├── output3.txt
│       ├── output4.txt
│       ├── output5.txt
│       ├── output6.txt
│       └── test_loup_chevre_chou.py
├── <font color="#12488B"><b>prog_dynamique</b></font>
│   └── <font color="#12488B"><b>eleves</b></font>
│       ├── laines.py
│       ├── <font color="#A347BA"><b>map_color_example.png</b></font>
│       ├── <font color="#A347BA"><b>map_color.png</b></font>
│       └── test_laines.py
├── <font color="#12488B"><b>rpn</b></font>
│   └── <font color="#12488B"><b>eleves</b></font>
│       ├── attendu1.txt
│       ├── attendu2.txt
│       ├── attendu3.txt
│       ├── attendu4.txt
│       ├── attendu5.txt
│       ├── attendu6.txt
│       ├── attendu7.txt
│       ├── input1.txt
│       ├── input2.txt
│       ├── input3.txt
│       ├── input4.txt
│       ├── input5.txt
│       ├── input6.txt
│       ├── input7.txt
│       ├── output1.txt
│       ├── output2.txt
│       ├── output3.txt
│       ├── output4.txt
│       ├── output5.txt
│       ├── output6.txt
│       ├── output7.txt
│       ├── rpn.py
│       └── test_rpn.py
├── <font color="#12488B"><b>carre</b></font>
│   └── <font color="#12488B"><b>carre.pdf</b></font>
└── <font color="#12488B"><b>sudoku</b></font>
    └── <font color="#12488B"><b>eleves</b></font>
        ├── attendu1.txt
        ├── attendu2.txt
        ├── attendu3.txt
        ├── attendu4.txt
        ├── input1.txt
        ├── input2.txt
        ├── input3.txt
        ├── input4.txt
        ├── output1.txt
        ├── output2.txt
        ├── output3.txt
        ├── output4.txt
        ├── <font color="#12488B"><b>ressources</b></font>
        │   └── CAPES_avril_2017_epreuve_info.pdf
        ├── sudoku.py
        └── test_sudoku.py
</pre>

Tous les  projets seront effectués par binôme à l'exception du projet `carre` qui pourra être réalisé en trinôme :

* Huit projets sont proposés dans l'archive, le premier `arbre_binaire` servira d'entraînement et sera corrigé en classe
* Un  ou plusieurs groupes pourront pourra choisir un projet libre développé avec le module [pyxel](https://github.com/kitao/pyxel). Dans ce cas, vous pourrez développer sur la plateforme [pyxelstudio](https://www.pyxelstudio.net/) et suivre les tutoriels suivants:
  * celui de M.Junier : <https://frederic-junier.org/NSI/premiere/Projets/Pyxel/decouverte_pyxel/>
  * ou le tutoriel officiel de la [Nuit du Code](https://www.nuitducode.net/) :

  <https://www.cahiernum.net/CGS8UD/>
   


Revenons aux sept projets de l'archive. Chaque répertoire de projet, à deux exceptions près, contient un unique sous-répertoire `eleves` avec la même structure. Par exemple, le répertoire  `sudoku\eleves` rassemble :

* un script python avec squelette de code à compléter `sudoku.py`
* un script python avec des tests unitaires : `test_sudoku.py` :
  * on peut exécuter automatiquement les tests depuis une ligne de commandes avec la commande  `pytest -v` si le module [pytest](https://pypi.org/project/pytest/) est installé :

<pre><font color="#26A269"><b>fjunier@fjunier</b></font>:<font color="#12488B"><b>~/projet_final_2023/sudoku/correction</b></font>$ pytest -v
<b>============================= test session starts ==============================</b>
platform linux -- Python 3.9.13, pytest-7.1.2, pluggy-1.0.0 -- /home/fjunier/anaconda3/bin/python
cachedir: .pytest_cache
rootdir: /home/fjunier/Git/Gitlab/frederic-junier/terminale-nsi/Projets/projet_final_2023/sudoku/correction
plugins: anyio-3.5.0
<b>collected 4 items                                                              </b>

test_sudoku.py::test1 <font color="#26A269">PASSED                                             [ 25%]</font>
test_sudoku.py::test2 <font color="#26A269">PASSED                                             [ 50%]</font>
test_sudoku.py::test3 <font color="#26A269">PASSED                                             [ 75%]</font>
test_sudoku.py::test4 <font color="#26A269">PASSED                                             [100%]</font>

<font color="#26A269">============================== </font><font color="#26A269"><b>4 passed</b></font><font color="#26A269"> in 1.56s ===============================</font>
</pre>

  * On peut aussi exécuter le fichier `test_sudoku.py` en mode interactif depuis une ligne de commandes et lancer les tests depuis l'interpréteur :

<pre>(base) <font color="#26A269"><b>fjunier@fjunier</b></font>:<font color="#12488B"><b>~/projet_final_2023/sudoku/correction</b></font>$ python3 -i test_sudoku.py 
&gt;&gt;&gt; test1()
Test sur input1.txt réussi
</pre>

  * Avec [pyzo](https://pyzo.org/install.html) on peut exécuter le fichier `test_sudoku.py` avec l'option `Run as script` pour bien régler le répertoire de travail, ensuite on peut lancer les tests dans la console Python voir même importer le module `pytest` s'il est installé et lancer `pytest.main()` depuis la console Python :
  

~~~
In [3]: test1()
Test sur input1.txt réussi

In [6]: pytest.main(['-v'])
============================= test session starts ==============================
platform linux -- Python 3.10.6, pytest-6.2.5, py-1.10.0, pluggy-0.13.0 -- /usr/bin/python3.10
cachedir: .pytest_cache
rootdir: /home/fjunier/Git/Gitlab/frederic-junier/terminale-nsi/Projets/projet_final_2023/sudoku/correction
collected 4 items                                                              

test_sudoku.py::test1 PASSED                                             [ 25%]
test_sudoku.py::test2 PASSED                                             [ 50%]
test_sudoku.py::test3 PASSED                                             [ 75%]
test_sudoku.py::test4 PASSED                                             [100%]

============================== 4 passed in 1.64s ===============================
Out[6]: <ExitCode.OK: 0>
~~~

   * Ave `Spyder` c'est plus complexe car la technologie utilisée  (QT) ne permet  pas de simuler toutes les fonctionnalités d'un terminal et en particulier pas la redirection de l'entrée standard (comme on le fait dans les tests pour brancher `input` sur le flux d'un fichier d'entrée à la place de la console). Un contournement possible est de positionner l'option `Run Configuration` sur  `Execute in an external system terminal` dans le menu  `Run`. Quand vous cliquerez sur `Run`, l'exécution du fichier sera déportée dans une fenêtre de terminal externe à Spyder où vous pourrez obtenir le même comportement que précédemment en appelant `test1()` par exemple.


## Choix d'un projet

Vous choisissez d'abord un partenaire de binôme puis vous vous positionnez sur un projet. Si plusieurs binômes souhaitent le même projet, ils sont départagés par l'enseignant en fonction de leur niveau ou à l'aide de la fonction `random.randint`.

Les septs sujets correspondent à des défis en ligne :

| Projet  |      Défi      | Niveau |Thème|
|----------|:-------------:|:-------------:|:-------------:|
| arbre_binaire | https://www.codingame.com/ide/puzzle/binary-search-tree-traversal|Facile|Parcours d'arbre|
| automate|  https://www.codingame.com/training/medium/elementary-cellular-automaton/solution  |Facile|Codage, parsing, dictionnaire|
| labyrinthe|  https://www.codingame.com/ide/puzzle/maze |Moyen moins|Parcours de graphe, Récursivité|
| loup_chevre_chou|  https://www.codingame.com/ide/puzzle/river-crossing|Moyen|Parcours de graphe en largeur|
| prog_dynamique|  https://pydefis.callicode.fr/defis/C22_ColorMap/txt|Difficile|Programmation dynamique|
|rpn|https://www.codingame.com/ide/puzzle/reverse-polish-notation|Moyen plus|Piles, Parsing,Exception|
|sudoku|https://www.codingame.com/ide/puzzle/sudoku-solver|Moyen plus|Pile ou Récursivité, Retour sur trace|
|carre|Extrait d'un TP donné en option info de CPGE MP|Difficile|Pile ou Récursivité, Retour sur trace, Paradigmes de programmation|

# Rendu

Déposez au plus tard le 10 mai sur la plateforme Eléa  une archive par groupe avec :

* tous les fichiers de tests  inputN.txt, outputN.txt et attenduN.txt (sauf les projets `carre` et `prog_dynamique`)
* le script projet.py complété et commenté
* le script de test test_projet.py non modifié

URL espace de dépôt : <https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=16243>



