from graphe import Graphe

def bfs_distance(sommet, graphe):
    decouvert = {s: False for s in graphe.sommets()}
    distance = {s:float('inf') for s in graphe.sommets()}
    en_attente = File()
    decouvert[sommet] = True
    distance[sommet] = 0
    en_attente.enfiler(sommet)
    while not en_attente.file_vide():
        s = en_attente.defiler()
        for v in graphe.voisins(s):
            if not decouvert[v]:
                decouvert[v] = True
                distance[v] = distance[s] + 1
                en_attente.enfiler(v)
    return distance

def test_bfs_distance():
    """Test unitaires pour bfs_distance"""
    # grapje non orienté
    g1 = Graphe(['s', 'a', 'b', 'c', 'e', 'd'])
    g1.ajoute_arc('s', 'a')
    g1.ajoute_arc('s', 'b')
    g1.ajoute_arc('a', 'c')
    g1.ajoute_arc('c', 'd')
    g1.ajoute_arc('b', 'c')
    g1.ajoute_arc('b', 'd')    
    g1.ajoute_arc('c', 'e')
    g1.ajoute_arc('d', 'e')
    assert bfs_distance('s', g1) == {'s': 0, 'a': 1, 'b': 1, 'c': 2, 'e': 3, 'd': 2}
    print("Test réussi")