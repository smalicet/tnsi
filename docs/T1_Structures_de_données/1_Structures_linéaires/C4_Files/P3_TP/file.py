class Cellule:
    
    def __init__(self, elt, suivant):
        self.element = elt
        self.suivant = suivant
        
class File:
    
    def __init__(self):
        self.debut = None
        self.fin = None
        
    def file_vide(self):
        return (self.debut is None) and (self.fin is None)

    def defiler(self):
        assert not self.file_vide(), "File Vide"
        elt = self.debut.element
        self.debut = self.debut.suivant
        if self.debut is None:
            self.fin = None
        return elt
      
    def enfiler(self, elt):
        if self.file_vide():
            self.fin = Cellule(elt, None)
            self.debut = self.fin
        else:
            self.fin.suivant = Cellule(elt, None)
            self.fin = self.fin.suivant
    
    # interface étendue 
    
    def queue(self):
        assert not self.file_vide()           
        file_queue = File()
        file_queue.debut = self.debut.suivant
        if self.debut.suivant is not None:
            file_queue.fin = self.fin
        else:
            file_queue.fin = file_queue.debut
        return file_queue
    
    def affichage_aux(self):
        if self.file_vide():
            return 'None'
        return f"{str(self.debut.element)} -> {self.queue().affichage_aux()}"
    
    def __str__(self):
        return "début : " + self.affichage_aux() + " : fin"
        
