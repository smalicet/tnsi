class File2:
    """Implémentation d'une file avec un tableau circulaire"""

    def __init__(self, taille_max):
        self.taille_max = taille_max
        self.tab = [None for _ in range(self.taille_max)]
        self.taille = 0
        self.debut = self.taille_max - 1
        self.fin = self.taille_max - 1
        
    def file_vide(self):
        return self.taille == 0

    def defiler(self):
        assert not self.file_vide(), "File Vide"
        elt = self.tab[self.debut]
        # à compléter
        ...
        
      
    def enfiler(self, elt):
        assert self.taille < self.taille_max, "Dépassement de capacité"
        self.fin = (self.fin + 1) % self.taille_max
        # à compléter
        ...
        
    # interface étendue 

    def __str__(self):
        sortie = "debut : " 
        k = self.debut
        while k != self.fin:
            sortie = sortie + str(self.tab[k]) + ' - '
            k = (k + 1) % self.taille_max
        sortie = sortie + str(self.tab[k]) +  ' - : fin'
        return sortie
        
def test_file2():
    f = File2(10)
    for k in range(1, 6):
        f.enfiler(k)
    for k in range(1, 6):
        assert f.defiler() == k
    assert f.file_vide()
    print("Tests réussis")
