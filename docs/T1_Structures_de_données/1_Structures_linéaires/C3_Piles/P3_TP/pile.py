#%% Interface A
# Implémentation fonctionnelle du type Pile avec un tableau dynamique Python

def creer_pile():
    """Renvoie une pile vide"""
    return []
    
def pile_vide(pile):
    """Teste si une pile est vide"""
    return pile == []
    
def depiler(pile):
    """Retire l'élément au sommet de la pile"""
    sommet = pile.pop()
    return sommet
    
def empiler(pile, elt):
    """Ajoute elt au sommet de la pile"""
    pile.append(elt)
    
def lire_sommet(pile):
    """Renvoie l'élement au sommet de la pile sans le retirer"""
    return pile[len(pile)  - 1]
    
def str_pile(pile):
    """Renvoie une chaîne de caractères représentant la pile
    avec le sommet à gauche"""
    tmp = creer_pile()    
    while not pile_vide(pile):
        empiler(tmp, depiler(pile))
    sortie = 'None'
    while not pile_vide(tmp):
        sommet = depiler(tmp)
        sortie = f'({sommet},{sortie})'
        empiler(pile, sommet)
    return sortie
 
#%% Interface B
## Implémentation du type Pile en POO avec une liste chaînée 
class Cellule:
    """Cellule pour liste chaînée"""
    
    def __init__(self, elt, suivant=None):
        self.element = elt
        self.suivant = suivant
        
class Pile:
    """Implémente le type abstrait Pile en POO avec une liste chaînée"""
    
    def __init__(self):
        """Constructeur, l'attribut contenu pointe vers la première cellule
        de la liste chaînée ou vers None si la pile est vide"""
        self.contenu = None

    def pile_vide(self):
        """Teste si la pile est vide"""
        return self.contenu is None

    def depiler(self):
        """Renvoie l'élement au sommet de la pile si elle est non vide
        Lève une erreur sinon"""
        assert not self.pile_vide(), "Pile Vide"
        sommet = self.contenu.element
        self.contenu = self.contenu.suivant
        return sommet

    def empiler(self, elt):
        """Ajoute elt au sommet de la pile"""
        if self.pile_vide():
            self.contenu = Cellule(elt)
        else:
            self.contenu = Cellule(elt, self.contenu)
    
    def lire_sommet(self):
        """Renvoie l'élément au sommet de la pile sans le retirer"""
        sommet = self.contenu.element
        return sommet
    
    def queue(self):
        assert not self.pile_vide()           
        pile_queue = Pile()
        pile_queue.contenu = self.contenu.suivant
        return pile_queue
    
    def __str__(self):
        if self.pile_vide():
            return 'None'
        return f"({str(self.contenu.element)},{str(self.queue())})"
