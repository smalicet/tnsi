class Cellule:
    """Cellule pour liste chainée"""
    
    def __init__(self, elt, suivant):
        self.element = elt
        self.suivant = suivant
        
class Pile_chaine:
    """Implémentation du type abstrait par liste chainée"""
    
    def __init__(self):
        # attribut pointant vers None sir pile vide
        # ou vers la première cellule de la liste chaînée
        self.contenu = None

    def pile_vide(self):
        return self.contenu is None

    def depiler(self):
        assert not self.pile_vide(), "Pile Vide"
        # à compléter
        ...
        

    def empiler(self, elt):
        if self.pile_vide():
            self.contenu = Cellule(elt, None)
        else:
            # à compléter
            ...
    
    # interface étendue
    def queue(self):
        assert not self.pile_vide()           
        pile_queue = Pile_chaine()
        pile_queue.contenu = self.contenu.suivant
        return pile_queue
    
    def __str__(self):
        if self.pile_vide():
            return 'None'
        return f"({str(self.contenu.element)},{str(self.queue())})"
    
def test_pile3():
    stack = Pile_chaine()
    for k in [8, 4, 3]:
        stack.empiler(k)
    assert str(stack) == '(3,(4,(8,None)))'
    for k in [3, 4, 8]:
        sommet = stack.depiler()
        assert sommet == k
    assert stack.pile_vide() == True
    print("tests réussis")