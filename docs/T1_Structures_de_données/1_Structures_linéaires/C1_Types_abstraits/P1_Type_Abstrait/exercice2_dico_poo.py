class Dico:
    
    def __init__(self, nb_clefs_max):
        self.pos = 0
        self.nb_clefs_max = nb_clefs_max
        self.tab = [None for _ in range(nb_clefs_max)]
        
    def ajouter(self, clef, valeur):
        assert self.pos < self.nb_clefs_max, "Dictionnaire plein"
        self.tab[self.pos] = (clef, valeur)
        self.pos = self.pos + 1
        
    def valeur(self, clef):
        for k in range(self.nb_clefs_max):
            c, v = self.tab[k]
            if c == clef:
                return v
        assert False, "clef pas dans le dictionnaire"
        
def test_dico2():
    # à compléter
    
    print("Tests réussis")
    