---
title:  Synthèses 🎯
---

# Synthèses 

## Programmes  officiels 

[📓 Programme en Première](https://eduscol.education.fr/document/30007/download){: .md-button}

[📓 Programme en Terminale](https://eduscol.education.fr/document/30010/download){: .md-button}



##  Synthèses  de cours du programme de terminale

### Thème 1 Structures de données



**Types abstraits**

* [Types abstraits](../../T1_Structures_de_données/1_Structures_linéaires/C1_Types_abstraits/P2_Synthèse/synthese_type_abstrait.md)

**Structures linéaires**

* [Listes](../../T1_Structures_de_données/1_Structures_linéaires/C2_Listes/Synthèse/synthese_liste.md)
* [Piles](../../T1_Structures_de_données/1_Structures_linéaires/C3_Piles/Synthèse/synthese_pile.md)
* [Files](../../T1_Structures_de_données/1_Structures_linéaires/C4_Files/Synthèse/synthese_file.md)

**Structures arborescentes**


* [Arbre binaire](../../T1_Structures_de_données/2_Structures_arborescentes/C1_Arbre_binaire/Synthèse/synthese_arbre_binaire.md)
* [Arbre binaire de recherche](../../T1_Structures_de_données/2_Structures_arborescentes/C2_Arbre_binaire_de_recherche/Synthèse/synthese_abr.md)

**Graphes**

* [Graphes](../../T1_Structures_de_données/3_Graphes/C0_Graphes/Synthèse/synthese_graphe.md)
* [Algorithmes sur les graphes](../../T1_Structures_de_données/3_Graphes/C1_Algorithmes_de_graphes/Synthèse/synthese_algo_graphe.md)

### Thème 2  Bases de données

* [Modèle relationnel](../../T2_BDD/C1_Modele_Relationnel/ressources/TNSI-Synthese-ModeleRelationnelV1-2022.pdf)
* [SQL](../../T2_BDD/C2_SQL/ressources/memento-sql-2022.pdf)

### Thème 3 Architecture, Système et Réseaux

* [Routage](../../T3_Archi_Sys_Réseau/C1_Routage/ressources/TNSI-Synthese-Routage-2022.pdf)
* [Processus](../../T3_Archi_Sys_Réseau/C2_Processus/ressources/TNSI-Synthese-Processus-2022.pdf)
* [SOC](../../T3_Archi_Sys_Réseau/C3_SOC/ressources/TNSI-SOC-Cours-2022V1.pdf)
* [Sécurisation des communications](../../T3_Archi_Sys_Réseau/C4_Sécurisation_communication/ressources/TNSI-Synthese-SecurisationCommunication-2022.pdf)
* [Système d'exploitation et ligne de commandes](../../T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_Systeme.md)


### Thème 4 Programmation


* [Programmation Orientée Objet](../../T4_Programmation/C1_POO/Synthèse/synthese_poo.md)
* [Récursivité](../../T4_Programmation/C2_Récursivité/Synthèse/synthese_recursivite.md)
* [Modularité et mise au point](../../T4_Programmation/C3_Modularité_et_mise_au_point/ressources/TNSI-Cours-Module-Test-2022.pdf)
* [Paradigmes](../../T4_Programmation/C4_Paradigmes/P1_Paradigmes.md)
* [Calculabilité](../../T4_Programmation/C5_Calculabilité/Synthèse/synthese_calculabilite.md)

### Thème 5 Algorithmique

* [Diviser Pour Régner](../../T5_Algo/1_Méthodes/C1_Diviser_pour_régner/Synthèse/synthese_dpr.md)
* [Algorithmes gloutons](../../T5_Algo/1_Méthodes/C2_Algorithmes_gloutons/Synthèse/synthese_gloutons.md)
* [Programmation dynamique](../../T5_Algo/1_Méthodes/C3_Programmation_dynamique/Synthèse/synthese_prog_dynamique.md)
* [Recherche textuelle](https://nuage03.apps.education.fr/index.php/s/Q6kpK4NCKzssL8S)