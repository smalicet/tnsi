---
title:  Epreuves terminales 🎯
---

!!! cite "Sources et crédits pour cette page"
    * [Site de Fabrice Nativel](https://fabricenativel.github.io/officiels/)
    * [Eduscol](https://eduscol.education.fr/727/detail-des-epreuves-du-baccalaureat-general#lien1)


## Programmes  officiels 

[📓 Programme en Première](https://eduscol.education.fr/document/30007/download){: .md-button}

[📓 Programme en Terminale](https://eduscol.education.fr/document/30010/download){: .md-button}



## Épreuve Terminale


!!! info "Objectifs de l'épreuve"
    > Source : [Note consolidée pour la session 2024](https://eduscol.education.fr/727/detail-des-epreuves-du-baccalaureat-general#lien1)

    L'épreuve porte sur le programme de l'enseignement de spécialité de la classe de terminale en vigueur. Les notions du programme de la classe de première en vigueur non approfondies en classe de terminale, doivent être connues et mobilisables. Elles ne peuvent cependant pas constituer un ressort essentiel du sujet.


!!! info "Nature de l'épreuve"
    > Source : [Note consolidée pour la session 2024](https://eduscol.education.fr/727/detail-des-epreuves-du-baccalaureat-general#lien1)

    L'épreuve terminale obligatoire de spécialité est composée de deux parties : une **partie écrite** et une **partie pratique**, chacune notée sur 20. La note de la partie écrite a un coefficient de 0,75 et celle de la partie pratique  a un coefficient de 0,25. La note globale de l'épreuve est donnée sur 20 points.

## Épreuve pratique du bac


!!! Note "Textes règlementaires"
    - [Modalités des épreuves de NSI au bac en 2020](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm?cid_bo=149244){target=_blank}
    - [Mise à jour MENE2330918N du 05/02/2024 pour les modalités d'évaluation](https://www.education.gouv.fr/bo/2024/Hebdo8/MENE2330918N)
    - [Note consolidée pour la session 2024](https://eduscol.education.fr/document/52908/download)


??? abstract "Modalités de l'épreuve pratique"

    - Durée : 1 heure
    - L'épreuve pratique donne lieu à une note sur **20 points**, qui sera ccompté avec un coefficient $0,25$ dans la note finale pour la spécialité NSI.

    La partie pratique consiste en la résolution de **deux exercices sur ordinateur**, chacun étant noté sur **10 points**. La Banque d'exercices est publique (disponible à partir du 12 mars 2024) :  [banque des épreuves pratiques](https://cyclades.education.gouv.fr/delos/public/listPublicECE).  Les exercices contenus dans cette banque ne sont pas dissociables. Le candidat traite les deux exercices proposés.

   

    Le candidat est évalué sur la base d'un dialogue avec un professeur-examinateur. Un examinateur évalue au maximum quatre élèves. L'examinateur ne peut pas évaluer un élève qu'il a eu en classe durant l'année en cours.


    **Premier exercice**

    Le premier exercice consiste à programmer un algorithme figurant explicitement au programme, ne présentant pas de difficulté particulière, dont on fournit une spécification. Il s'agit donc de restituer un algorithme rencontré et travaillé à plusieurs reprises en cours de formation. Le sujet peut proposer un jeu de test avec les réponses attendues pour permettre au candidat de vérifier son travail.


    **Deuxième exercice**

    Pour le second exercice, un programme est fourni au candidat. Cet exercice ne demande pas l'écriture complète d'un programme, mais permet de valider des compétences de programmation suivant des modalités variées : le candidat doit, par exemple, compléter un programme « à trous » afin de répondre à une spécification donnée, ou encore compléter un programme pour le documenter, ou encore compléter un programme en ajoutant des assertions, etc.


    **Grille d'évaluation**

    Un exemple (caractère non obligatoire) est proposé [ici](https://www.education.gouv.fr/bo/2024/Hebdo8/MENE2330918N), chaque corrrecteur construit une grille sur ce modèle.

    ![alt](https://nuage03.apps.education.fr/index.php/s/iJqEwJNWjtc5xNE/download/grille_evaluation.png){: .center}






!!! tip "Se préparer"
    ⚠️ _Les sujets publics de l'épreuve pratique ne doivent pas donner lieu à un bachotage en classe._

    Les sujets sont disponibles sur le site de la [banque d'épreuves publique](https://cyclades.education.gouv.fr/delos/public/listPublicECE).

    [Script Python de téléchargement des 48 sujets](./script_ep/epreuve_pratique_download2.py)

    [Archive avec les 48 sujets](https://nuage03.apps.education.fr/index.php/s/sFFYwBxp5ykjAY8)

    [Carnet Capytale avec les corrigés des 48 sujets](https://capytale2.ac-paris.fr/web/c/20ae-3202587)

    Quelques sites pour s'entraîner sur les sujets et  des  corrigés  :
    
    * [Site de Gilles Lassus](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2024/)
  
        |Année|Énoncés et corrigés sur le site de Gilles Lassus|
        |:---:|:---:|
        |2021|[lien](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2021/)|
        |2022|[lien](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2022/)|
        |2023|[lien](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2023/)|
        |2024|[lien](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2024/)|

    * [Site de Fabrice Nativel](https://fabricenativel.github.io/index_annales/)
    * [Site de David Roche](https://pixees.fr/informatiquelycee/term/#ep_prat) : défis basés sur la plateforme [codepuzzle.io](https://www.codepuzzle.io/defis-banque) de Laurent Abbal.
    * [Site de Laura Fléron](https://lfleron.forge.aeif.fr/nsi_ep2023/) avec analyse des thèmes et difficultés des sujets et des correspondances entre les années.
    * Laurent Abbal a mis en ligne les sujets de l'épreuve pratique 2023 sous forme de Défis : [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque)
    *  [codex.forge.apps.education.fr](https://codex.forge.apps.education.fr) : Ce site propose des exercices d'apprentissage de l'algorithmique et de la programmation par le biais d'exercices variés. Le langage utilisé est Python. Les exercices proposés ont été écrits, testés, corrigés et améliorés par des professeurs d'informatique du secondaire et du supérieur.


## Épreuve écrite du bac


!!! Note "Textes règlementaires"
    - Le [texte officiel de 2020 relatif aux modalités de l'épreuve en NSI](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm).
    - [Mise à jour du 26/09/2023 (page 52)](https://www.education.gouv.fr/sites/default/files/Bulletin_officiel_MENJS_2023_09_28_BO36-1695914069.pdf)
    - [Note consolidée pour la session 2024](https://eduscol.education.fr/document/52908/download)


??? abstract "Modalités de l'épreuve écrite"
    Durée : 3 heures 30.

    Le sujet comporte trois exercices indépendants les uns des autres, qui permettent d'évaluer les connaissances et compétences des candidats.
    

!!! tip "Quelques sites pour s'entraîner"
    Les sujets avec leurs corrigés sont disponibles sur :

    * [Site de David Roche](https://pixees.fr/informatiquelycee/term/#suj_bac)
    * [Site de Fabrice Nativel](https://fabricenativel.github.io/index_annales/)



## Le Grand Oral



!!! Note "Ressources officielles"
    * [Page Eduscol du Grand Oral](https://eduscol.education.fr/729/presentation-du-grand-oral)
    * [FAQ du Grand Oral](https://eduscol.education.fr/document/7139/download?attachment) ➡️ à lire
    * [Infographie pour les élèves](https://eduscol.education.fr/document/53952/download?attachment)
    * [Infographie à propos du support](https://eduscol.education.fr/document/44140/download)

??? abstract "Modalités du Grand Oral"


    **Pendant 10 minutes,** le candidat présente la question choisie et y répond. Le jury évalue son argumentation et ses qualités de présentation. Pendant son exposé, sauf aménagements pour les candidats à besoins spécifiques, le candidat est debout. Pour son exposé, le candidat peut s'appuyer sur son support. Il peut le montrer au jury mais pas le lui remettre.

    Ensuite,**pendant 10 minutes,** le jury échange avec le candidat et évalue la solidité de ses connaissances et ses compétences argumentatives. Ce temps d'échange permet à l'élève de mettre en valeur ses connaissances, liées au programme de la spécialité suivie en classes de première et de terminale, sur laquelle repose la question présentée pendant la première partie de l'épreuve (ou sur les deux spécialités suivies en classes de première et de terminale lorsque la question présentée pendant la première partie de l'épreuve était une question transversale). Durant l'échange, le candidat peut s'appuyer sur son support. Il peut le montrer au jury, à son initiative ou en réponse à une question du jury. Pour autant, le jury ne peut pas le conserver à l'issue de l'épreuve ni l'évaluer. Le candidat dispose d'un tableau dans la salle d'examen, qu'il peut utiliser, s'il le souhaite, durant ce deuxième temps. En revanche, le jury ne peut pas demander au candidat d'écrire (ni sur une feuille, ni au tableau) pour répondre à des questions qu'il lui soumettrait ou faire des exercices.

!!! tip "Quelques ressources pour se préparer"
    *  [Diaporama de présentation du Grand Oral 2024](https://nuage03.apps.education.fr/index.php/s/amf8BsMbDot9dF8)
    *  [Programme de terminale NSI](https://eduscol.education.fr/document/30010/download)    
    *  [Liste de ressources pour le Grand Oral](tps://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/page/view.php?id=1741)
    *  [Boîte à outils pour le Grand Oral (sur Moodle)](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
    *  [https://www.mon-oral.net/](https://www.mon-oral.net/) un site pour s'enregistrer et partager ses capsules audio avec son enseignant
    *  [Ressources sur Moodle/Elea](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)