#Sans liste : 
for n in range(15):
    print(3*n + 5)
print()

#Méthode 1:
S1 = []
for n in range(15):
    S1 = S1 + [3*n + 5]
print('S1 =',S1)
print()

#Méthode 2:
S2 = []
for n in range(15):
    S2.append(3*n + 5)
print('S2 =',S2)
print()

#Méthode 3:
S3 = [ 3*n + 5 for n in range(15)]
print('S3 =',S3)
print()

#Méthode 4:
S4 = [0] * 15
for n in range(15):
    S4[n] = 3*n + 5
print('S4 =',S4)
print()    

    
