# But : Présenter un projet pensé et élaborer en équipe

Le but de ce projet est de présenter votre façon de travailler en groupe, tout en vous aventurant dans un thème pouvant vous servir lors du grand Oral du baccalauréat.

### Quel type de projet ?

Le projet peut prendre différentes formes :
* Programmes en python
* Application smartphone
* Web
* Constructions / Expériences
* Travaux de recherche

L'important étant que chaque élève du groupe apporte quelque chose par son expérience, ses compétences ou connaissances.  

## Evaluation

- 10 points sur le rendu final du projet.
- 10 points sur la présentation du projet :
    * Rôle de chacun
    * Rapport des brainstormings
    * Evolution du projet (les différentes étapes, un carnet de bord ? ...)
    * Problèmes rencontrés / Adaptabilité face au Problèmes
    * Liens possibles avec le cours de NSI
    * Bilan