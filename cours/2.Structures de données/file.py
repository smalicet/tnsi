class File:
    ''' classe File
    création d’une instance File avec une liste
    '''
    def __init__(self):
        '''Initialisation d’une file vide'''
        self.L = []
    def __str__(self):
        return str(self.L)
    def vide(self):
        '''teste si la file est vide'''
        return self.L == []
    def taille(self):
        return len(self.L)
    def sommet(self):
        return self.L[0]
    def defiler(self):
        '''défile'''
        if not self.vide():
            x = self.L[0]
            self.L = self.L[1:]
            return x
    def enfiler(self,x):
        '''enfile'''
        return self.L.append(x)
