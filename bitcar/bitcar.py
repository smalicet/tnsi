from microbit import *
import radio

radio.config(channel=12)
radio.on()

while True:
    message = radio.receive()
    if message == 'A':
        pin13.write_analog(0)
        pin14.write_analog(1023)
        pin15.write_analog(1023)
        pin16.write_analog(0)
    elif message == 'D':
        pin13.write_analog(500)
        pin14.write_analog(0)
        pin15.write_analog(0)
        pin16.write_analog(500)
    elif message == 'C':
        pin13.write_analog(100)
        pin14.write_analog(0)
        pin15.write_analog(300)
        pin16.write_analog(0)
    elif message == 'B':
        pin13.write_analog(0)
        pin14.write_analog(300)
        pin15.write_analog(0)
        pin16.write_analog(100)
    elif message == 'R':
        pin13.write_analog(0)
        pin14.write_analog(0)
        pin15.write_analog(0)
        pin16.write_analog(0)