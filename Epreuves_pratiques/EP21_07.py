# 1
def fibo(n):
    tab = [1] * (n+1)
    for i in range(3, n+1):
        tab[i] = tab[i-1] + tab[i-2]
    return tab[n]

assert fibo(1) == 1
assert fibo(2) == 1
assert fibo(25) == 75025
assert fibo(45) == 1134903170




# 2
liste_eleves = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
liste_notes = [1, 40, 80, 60, 58, 80, 75, 80, 60, 24]

def meilleures_notes():
    note_maxi = 0
    nb_eleves_note_maxi = 0
    liste_maxi = []
    
    for compteur in range(len(liste_notes)):
        if liste_notes[compteur] == note_maxi:
            nb_eleves_note_maxi = nb_eleves_note_maxi + 1
            liste_maxi.append(liste_eleves[compteur])
        if liste_notes[compteur] > note_maxi:
            note_maxi = liste_notes[compteur]
            nb_eleves_note_maxi = 1
            liste_maxi = [liste_eleves[compteur]]
            
    return (note_maxi, nb_eleves_note_maxi, liste_maxi)

assert meilleures_notes() == (80, 3, ['c', 'f', 'h'])
