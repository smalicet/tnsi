# 1
def taille(arbre, lettre):
    if arbre[lettre][0] == '' and arbre[lettre][1] == '':
        return 1
    elif arbre[lettre][0] == '':
        return 1 + taille(arbre, arbre[lettre][1])
    elif arbre[lettre][1] == '':
        return 1 + taille(arbre, arbre[lettre][0])
    else:
        return 1 + taille(arbre, arbre[lettre][0]) + taille(arbre, arbre[lettre][1])

a = {'F': ['B', 'G'], 'B': ['A', 'D'], 'A': ['', ''], 'D': ['C', 'E'], \
     'C': ['', ''], 'E': ['', ''], 'G': ['', 'I'], 'I': ['', 'H'], 'H': ['', '']}
assert taille(a, 'F') == 9

# ATTENTION ! la fonction taille ne gère pas les arbres vides
def taille2(arbre, lettre):
    if lettre == '':
        return 0
    else:
        return 1 + taille2(arbre, arbre[lettre][0]) + taille2(arbre, arbre[lettre][1])




# 2
def tri_iteratif(tab):
    for k in range(len(tab)-1, 0, -1):
        imax = k
        for i in range(0, k):
            if tab[i] > tab[imax]:
                imax = i
        if tab[imax] > tab[k]:    # ATTENTION ! erreur dans l'indice de tab !
            tab[k], tab[imax] = tab[imax], tab[k]
    return tab

# ATTENTION ! assert faux !
assert tri_iteratif([41, 55, 21, 18, 12, 6, 25]) == [6, 12, 18, 21, 25, 41, 55]
