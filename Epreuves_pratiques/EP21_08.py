# 1
def recherche(caractere, mot):
    occ = 0
    for c in mot:
        if c == caractere:
            occ += 1
    return occ

assert recherche('e', "sciences") == 2
assert recherche('i', "mississippi") == 4
assert recherche('a', "mississippi") == 0




# 2
Pieces = [100, 50, 20, 10, 5, 2, 1]
def rendu_glouton_r(arendre, solution=[], i=0):    # ATTENTION : nom de la fonction erronné !
    if arendre == 0:        # ATTENTION, problème d'indentation !
        return solution     # ATTENTION, problème d'indentation !
    p = Pieces[i]           # ATTENTION : nom de la variable erronné !
    if p <= arendre:
        solution.append(Pieces[i])    # ATTENTION : ajout sinon script faux sans l'alternative pour "return"
        return rendu_glouton_r(arendre - p, solution, i)    # ATTENTION : nom de la fonction erronné !
# ou alors return rendu_glouton_r(arendre - p, solution + [Pieces[i]], i)
    else:
        return rendu_glouton_r(arendre, solution, i+1)    # ATTENTION : nom de la fonction erronné !

assert rendu_glouton_r(68, [], 0) == [50, 10, 5, 2, 1]
assert rendu_glouton_r(291, [], 0) == [100, 100, 50, 20, 20, 1]
