# 1
def recherche(elt, tab):
    i = 0
    while i < len(tab) and tab[i] != elt:
        i += 1
    if i == len(tab):
        i = -1
    return i

assert recherche(1, [2, 3, 4]) == -1
assert recherche(1, [10, 12, 1, 56]) == 2
assert recherche(50, [1, 50, 1]) == 1
assert recherche(15, [8, 9, 10, 15]) == 3


# 2
def insere(a, tab):
    l = list(tab) #l contient les mêmes éléments que tab
    l.append(a)
    i = len(l) - 2
    while a < l[i] and i > -1:    # ATTENTION ! 2e test non demandé mais obligatoire !
        l[i+1] = l[i]     # ATTENTION ! problèmes d'indentation !
        l[i] = a          # ATTENTION ! problèmes d'indentation !
        i = i-1           # ATTENTION ! problèmes d'indentation !
    return l

assert insere(3, [1, 2, 4, 5]) == [1, 2, 3, 4, 5]
assert insere(10, [1, 2, 7, 12, 14, 25]) == [1, 2, 7, 10, 12, 14, 25]
assert insere(1, [2, 3, 4]) == [1, 2, 3, 4]
