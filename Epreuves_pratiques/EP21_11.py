# 1
def conv_bin(n):
    b = [n%2]
    n = n //2
    while n > 0:
        b = b + [n%2]
        n = n // 2
    return b, len(b)

assert conv_bin(13) == ([1, 0, 1, 1], 4)

# ATTENTION : assert faux !




# 2
def tri_bulles(T):
    n = len(T)
    for i in range(n-1, 0, -1):
        for j in range(i):
            if T[j] > T[j+1]:
                temp = T[j]
                T[j] = T[j+1]
                T[j+1] = temp
    return T

# ATTENTION assert ajouté
L0 = [4, 9, 5, 1, 0, 10, -1]
L1 = list(L0)
assert tri_bulles(L0) == sorted(L1)


def tri_bulles2(T):
    n = len(T)
    for i in range(n-1):
        for j in range(i, -1, -1):
            if T[j] > T[j+1]:
                temp = T[j]
                T[j] = T[j+1]
                T[j+1] = temp
    return T

# ATTENTION assert ajouté
L0 = [4, 9, 5, 1, 0, 10, -1]
L1 = list(L0)
assert tri_bulles2(L0) == sorted(L1)
