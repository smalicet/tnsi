# 1
def rendu(somme_a_rendre):
    assert somme_a_rendre != 0
    pieces = [5, 2, 1]
    monnaie = [0, 0, 0]
    s = somme_a_rendre
    for i in range(3):
        while pieces[i] <= s:
            monnaie[i] += 1
            s -= pieces[i]
    return monnaie

assert rendu(13) == [2, 1, 1]
assert rendu(64) == [12, 2, 0]
assert rendu(89) == [17, 2, 0]

# ATTENTION : dernier assert donné FAUX ! [17, 2, 1]




# 2
class Maillon:
    def __init__(self, v, s=None):    # ATTENTION manque 2e paramètre !
        self.valeur = v
        self.suivant = s

class File:
    def __init__(self):
        self.dernier_file = None

    def enfile(self, element):
        nouveau_maillon = Maillon(element, self.dernier_file)
        self.dernier_file = nouveau_maillon

    def est_vide(self):
        return self.dernier_file == None

    def affiche(self):
        maillon = self.dernier_file
        while maillon != None:
            print(maillon.valeur)
            maillon = maillon.suivant

    def defile(self):
        if not self.est_vide():
            if self.dernier_file.suivant == None:
                resultat = self.dernier_file.valeur
                self.dernier_file = None
                return resultat
            maillon = self.dernier_file
            while maillon.suivant.suivant != None:
                maillon = maillon.suivant
            resultat = maillon.suivant.valeur
            maillon.suivant = None
            return resultat
        return None

F = File()
assert F.est_vide() == True
# True
F.enfile(2)
F.affiche()
# 2
assert F.est_vide() == False
F.enfile(5)
F.enfile(7)
F.affiche()
# 7 5 2
assert F.defile() == 2
assert F.defile() == 5
F.affiche()
# 7
