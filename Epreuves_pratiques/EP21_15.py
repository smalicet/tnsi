# 1
def RechercheMinMax(tab):
    if tab == []:
        dico = {'min': None, 'max': None}
    else:
        dico = {'min': tab[0], 'max': tab[0]}
        for el in tab:
            if el < dico['min']:
                dico['min'] = el
            if el > dico['max']:
                dico['max'] = el
    return dico

tableau = [0, 1, 4, 2, -2, 9, 3, 1, 7, 1]
resultat = RechercheMinMax(tableau)    # ATTENTION ! nom de la fonction erroné
assert resultat == {'min': -2, 'max': 9}
tableau = []
resultat = RechercheMinMax(tableau)    # ATTENTION ! nom de la fonction erroné
assert resultat == {'min': None, 'max': None}




# 2
class Carte:
    """Initialise Couleur (entre 1 à 4), et Valeur (entre 1 à 13)"""
    def __init__(self, c, v):
        assert (c > 0 and c < 5)
        assert (v > 0 and v < 14)
        self.Couleur = c
        self.Valeur = v

    """Renvoie le nom de la Carte As, 2, ... 10,
       Valet, Dame, Roi"""
    def getNom(self):
        if (self.Valeur > 1 and self.Valeur < 11):
            return str(self.Valeur)
        elif self.Valeur == 11:
            return "Valet"
        elif self.Valeur == 12:
            return "Dame"
        elif self.Valeur == 13:
            return "Roi"
        else:
            return "As"

    """Renvoie la couleur de la Carte (parmi pique, coeur, carreau, trefle"""
    def getCouleur(self):
        return ['pique', 'coeur', 'carreau', 'trefle'][self.Couleur-1]   # ATTENTION ! problème d'indice !

class PaquetDeCarte:
    def __init__(self):
        self.contenu = []

    """Remplit le paquet de cartes"""    # ATTENTION ! consignes très imprécise !
    def remplir(self):
        for c in range(1, 5):
            for v in range(1, 14):
                self.contenu = self.contenu + [Carte(c, v)]

    """Renvoie la Carte qui se trouve à la position donnée"""
    def getCarteAt(self, pos):
        assert pos < len(self.contenu)+1
        return self.contenu[pos-1]

unPaquet = PaquetDeCarte()
unPaquet.remplir()
uneCarte = unPaquet.getCarteAt(20)
print(uneCarte.getNom() + " de " + uneCarte.getCouleur())
# 6 de coeur
