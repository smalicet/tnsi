# 1
def indice_du_min(tab):    # ATTENTION ! nom de la fonction erroné
    ind = 0
    for i in range(1, len(tab)):
        if tab[i] < tab[ind]:
            ind = i
    return ind

assert indice_du_min([5]) == 0
assert indice_du_min([2, 4, 1]) == 2
assert indice_du_min([5, 3, 2, 2, 4]) == 2




# 2
def separe(tab):
    i = 0
    j = len(tab)-1
    while i < j:
        if tab[i] == 0:
            i = i + 1
        else:
            tab[i], tab[j] = tab[j], tab[i]
            j = j - 1
    return tab

assert separe([1, 0, 1, 0, 1, 0, 1, 0]) == [0, 0, 0, 0, 1, 1, 1, 1]
assert separe([1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0]) == [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1]

