# 1
def tri_selection(tab):
    n = len(tab)
    for j in range(n-1):
        ind = j
        for i in range(j+1, n):
            if tab[i] < tab[ind]:
                ind = i
        tmp = tab[j]
        tab[j] = tab[ind]
        tab[ind] = tmp
    return tab

assert tri_selection([1, 52, 6, -9, 12]) == [-9, 1, 6, 12, 52]




# 2
from random import randint

def plus_ou_moins():
    nb_mystere = randint(1, 99)   # ATTENTION ! manque a et b "inclus" dans le texte !
    nb_test = int(input("Proposez un nombre entre 1 et 99 : "))
    compteur = 1

    while nb_mystere != nb_test and compteur < 10:
        compteur = compteur + 1
        if nb_mystere > nb_test:
            nb_test = int(input("Trop petit ! Testez encore : "))
        else:
            nb_test = int(input("Trop grand ! Testez encore : "))

    if nb_mystere == nb_test:
        print("Bravo ! Le nombre était ", nb_mystere)
        print("Nombre d'essais: ", compteur)
    else:
        print("Perdu ! Le nombre était ", nb_mystere)
