# 1
def maxi(tab):
    m = tab[0]
    ind = 0
    for i in range(1, len(tab)):
        if tab[i] > m:
            m = tab[i]
            ind = i
    return m, ind

assert maxi([1, 5, 6, 9, 1, 2, 3, 7, 9, 8]) == (9, 3)

# ATTENTION : rien de précisé si tab est vide




# 2
def recherche(gene, seq_adn):
    n = len(seq_adn)
    g = len(gene)
    i = 0
    trouve = False
    while i < n-g+1 and trouve == False:
        j = 0
        while j < g and gene[j] == seq_adn[i+j]:
            j += 1
        if j == g:
            trouve = True
        i += 1
    return trouve

assert recherche("AATC", "GTACAAATCTTGCC") == True
assert recherche("AGTC", "GTACAAATCTTGCC") == False
